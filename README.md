# AMBLE #



### What is AMBLE? ###

**AMBLE** (**A**PEC **M**obility & **B**arrier-free Travel for **L**ocal **E**conomy) is a web app that provides updated COVID-cognizant human gathering & travel data 
about local communities within the global context of **APEC** (**A**sia-**P**acific **E**conomic **C**ooperation) member economies. 